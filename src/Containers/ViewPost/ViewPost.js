import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Image, Panel} from "react-bootstrap";
import {fetchPost} from "../../store/actions/posts";
import config from "../../config";
import textImage from '../../assets/images/text_image.png';
import Comments from "../../Components/Comments/Comments";

class ViewPost extends Component {

  componentDidMount() {
    const postId = this.props.match.params.id;
    this.props.onFetchPost(postId);
  }

  renderPost = isPost => {
    let post = isPost;
    let isImage = textImage;

    if (post.image) {
      isImage = config.apiUrl + '/uploads/' + post.image;
    }

    return(
      <Panel>
        <Panel.Heading style={{display: 'flex', justifyContent: 'space-between'}}>
          <span><b>About:</b> {post.title} </span>
          <span>Author: <b>{post.author}</b></span>
          <span><b>created:</b> {post.datetime}</span>
        </Panel.Heading>
        <Panel.Body>
          <Image style={{width: '150px', marginRight: '20px'}}
                 rounded
                 src={isImage}/>
          <p style={{wordWrap: 'break-word'}}>{post.description}</p>
        </Panel.Body>
        <Panel.Footer>
          <Panel.Title style={{marginBottom: '20px'}}><b>Comments</b></Panel.Title>
          <Comments postId={this.props.match.params.id}/>
        </Panel.Footer>
      </Panel>
    );
  };

  render() {
    return(
      <Fragment>
        {this.props.isPost
          ? this.renderPost(this.props.isPost)
          : null
        }
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  isPost: state.posts.post,
});

const mapDispatchToProps = dispatch => ({
  onFetchPost: (postId) => dispatch(fetchPost(postId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewPost);