import React from 'react';
import {Nav, Navbar, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import UserMenu from "../Menus/UserMenu";
import AnonymousMenu from "../Menus/AnonymousMenu";

const Toolbar = ({user, logout}) => (
  <Navbar>
    <Navbar.Header>
      <Navbar.Brand>
        <LinkContainer to="/" exact><a>Forum</a></LinkContainer>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav>
        <LinkContainer to="/" exact>
          <NavItem>Posts</NavItem>
        </LinkContainer>
      </Nav>
        {user
          ? <UserMenu logout={logout} user={user}/>
          : <AnonymousMenu/>
        }
    </Navbar.Collapse>
  </Navbar>
);

export default Toolbar;