import axios from '../../axios-api';
import {CREATE_POST_SUCCESS, FETCH_POST_SUCCESS, FETCH_POSTS_SUCCESS} from "./actionTypes";
import {push} from "react-router-redux";

export const fetchPosts = () => {
  return dispatch => {
    axios.get('/posts').then(
      response => dispatch(fetchPostsSuccess(response.data))
    );
  }
};

const fetchPostsSuccess = posts => {
  return {type: FETCH_POSTS_SUCCESS, posts};
};

export const fetchPost = (postId) => dispatch => {
  axios.get(`/posts/${postId}`)
    .then(res => dispatch(fetchPostSuccess(res.data)));
};

const fetchPostSuccess = post => {
  return {type: FETCH_POST_SUCCESS, post};
};

export const createPostSuccess = () => {
  return {type: CREATE_POST_SUCCESS};
};

export const createPost = (postData, token) => dispatch => {
  const headers = {"Token": token};

  axios.post('/posts', postData, {headers}).then(
    response => {
      dispatch(createPostSuccess());
      dispatch(push('/'));
    }
  );
};