export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';


export const CREATE_POST_SUCCESS = 'CREATE_POST_SUCCESS';


export const FETCH_POST_SUCCESS = 'FETCH_POST_SUCCESS';


export const SUCCESS_SEND_COMMENT = 'SUCCESS_SEND_COMMENT';


export const SUCCESS_FETCH_COMMENTS = 'SUCCESS_FETCH_COMMENTS';



export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';
export const REGISTER_USER_FAILURE = 'REGISTER_USER_FAILURE';

export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';
export const LOGIN_USER_FAILURE = 'LOGIN_USER_FAILURE';

export const LOGOUT_USER = 'LOGOUT_USER';