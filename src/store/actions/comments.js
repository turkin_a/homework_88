import axios from '../../axios-api';
import {SUCCESS_FETCH_COMMENTS} from "./actionTypes";

export const sendComment = (comment, token, postId) => dispatch => {
  const headers = {"Token": token};

  axios.post(`/comments`, comment, {headers})
    .then(() => dispatch(fetchComments(postId)));
};

export const fetchComments = postId => async dispatch => {
  let comments = await axios.get(`/comments/${postId}`);

  dispatch(successFetchComments(comments.data));
};

const successFetchComments = comments => {
  return {type: SUCCESS_FETCH_COMMENTS, comments};
};