import {SUCCESS_FETCH_COMMENTS} from "../actions/actionTypes";

const initialState = {
  comments: [],
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case SUCCESS_FETCH_COMMENTS:
      return {...state, comments: action.comments};
    default:
      return state;
  }
};

export default reducer;