export const saveState = state => {
  try {
    // console.log('SAVING STATE to local storage')
    const serializedState = JSON.stringify(state);
    localStorage.setItem('state', serializedState);
    // console.log('STATE WAS saved from local storage')
  } catch (e) {
    console.log('Could not save state');
  }
};

export const loadState = () => {
  try {
    // console.log('LOADING STATE from LOCAL STORAGE')
    const serializedState = localStorage.getItem('state');
    // console.log('serializedState ===', serializedState);

    if (serializedState === null) {
      // console.log('STATE === NULL');
      return undefined;
    }

    // console.log('STATE WAS loaded from LOCAL STORAGE')
    return JSON.parse(serializedState);
  } catch (e) {
    return undefined;
  }
};